# OpenML dataset: Snapchat-Google-PlayStore-Reviews

https://www.openml.org/d/43575

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The dataset contains reviews from google playstore on snapchat.
With the sentiment analysis, we can check for the users' adoption of andriod version of snapchat, which has been improved significantly recently (as claimed by the company). Also whether the app is redefining  the  use of camera and changing the way people interact with their close ones.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43575) of an [OpenML dataset](https://www.openml.org/d/43575). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43575/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43575/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43575/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

